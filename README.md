# Project description

Its firmware repository for the "Phase controller" project which consist of custom made electronics.
Its main purpose is to control AC 1-phase power supplied devices like motors or bulbs.

## Device description

### System Hardware
- Hardware electronics repository [PCB repo](https://gitlab.com/bartoszmichalak19/phase-regulator-hw)
- Drawing of the device system![system HW architecture](doc/arch/system_HW.drawio.png)

### System SW
- General view drawing ![system SW architecture](doc/arch/system-SW.drawio%20.png)
- More detailed design is [here](doc/arch/plantuml/general_design.iuml)

## Getting started
Just run `release.sh` or `debug.sh` to build firmware and `flash.sh` to deploy it. To be done more common deploying scripts.

