#!/bin/bash
mkdir -p build
mkdir -p build/release
cmake -B build/release -DCMAKE_BUILD_TYPE=Release
cd build/release
make -j8 all
