#!/bin/bash
mkdir -p build
mkdir -p build/unit_tests
cmake -DBUILD_UT=ON -S unit_tests -B build/unit_tests 
cd build/unit_tests
make -j8 all && ctest --output-on-failure