#!/bin/bash
echo "------------|Patching stm32 bsp sdk|-----------------"
patch -N -r - --silent --forward bsp/cube/Core/Src/stm32f4xx_it.c bsp/patches/stm32f4xx_it_c.patch
patch -N -r - --silent --forward bsp/cube/Core/Src/stm32f4xx_hal_msp.c bsp/patches/stm32f4xx_hal_msp_c.patch