/**
 * @file main.cpp
 * @author Bartosz Michalak
 * @brief Currently main is used for evaluation of new core features
 * @version 0.1
 * @date 2022-07-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "main.h"
#include "cmsis_os.h"
#include "main_controller.hpp"
#include "phase_controller.hpp"
#include "status_indicator.hpp"
#include "rtos_task.hpp"
#include "log_flusher.hpp"
#include "rtos_queue.hpp"
#include "logger.hpp"
#include "i_logger.hpp"
#include "logs_receiver.hpp"
#include "logs_transmitter.hpp"
#include "zero_ten_volt_input.hpp"
#include "event_queue.hpp"
#include "adc.hpp"

template<size_t buf_size>
class foo
{
    Queue<int,5>& q_;
    ILogger& logger_;
public:
    foo(Queue<int,5>& q, ILogger& l = pfc::Logger::getInstance()) : q_(q), logger_(l){}
    void init()
    {
        dummyTaskHandle = osThreadCreate(&os_thread_def_dummyTask, nullptr);
        logger_.debug("elo debug");
    }

    static void dummyTask(void const *argument)
    {
        for (;;)
        {
            HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
            vTaskDelay(50);
        }
    }

private:
    osThreadId dummyTaskHandle;
    std::array<uint32_t, buf_size> dummyTaskBuffer;
    osStaticThreadDef_t dummyTaskControlBlock;
    const osThreadDef_t os_thread_def_dummyTask = {const_cast<char *>("dummyTask"), dummyTask, osPriorityNormal, 0, buf_size, dummyTaskBuffer.data(), &dummyTaskControlBlock};
};

constexpr const char* log_flusher_task_name = "LogFlusher";
const osPriority log_flusher_task_priority = osPriorityNormal;
const size_t log_flusher_task_stack_size = 1024;

constexpr const char* zero_ten_volt_task_name = "ZeroTenVoltInputSignal";
const osPriority zero_ten_volt_task_priority = osPriorityNormal;
const size_t zero_ten_volt_task_stack_size = 256;

constexpr const char* main_controller_task_name = "MainController";
const osPriority main_controller_task_priority = osPriorityNormal;
const size_t main_controller_task_stack_size = 256;

void fun(Queue<int,5>& q)
{
    q.push(2);
}

int main(void)
{

    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_TIM2_Init();
    MX_USART1_UART_Init();
    MX_ADC1_Init();
    
    pfc::Logger::getInstance().info("info_dummy_log");
    pfc::Logger::getInstance().error("info error log");

    static Queue<int,5> q;
    fun(q);
    q.push(1);

    static foo<512> f(q);
    f.init();

    static pfc::LogsTransmitter logs_tx;
    static auto logs_rx = pfc::LogsReceiver(pfc::Logger::getInstance().getLogsQueue());
    static auto log = LogFlusher(logs_rx, logs_tx, pfc::Logger::getInstance());
    static auto log_flusher_task =
        RtosTask<log_flusher_task_stack_size>(log_flusher_task_name,log_flusher_task_priority, log);
    log_flusher_task.create();


    static PhaseController pfc;
    static StatusIndicator si;
    static pfc::Adc zero_ten_volt_adc{ADC_CHANNEL_1};
    static pfc::EventQueue<uint8_t,1> setpoints_q;
    static pfc::ZeroTenVoltInput zero_ten_input{zero_ten_volt_adc, pfc::Logger::getInstance(),setpoints_q};
    static auto zero_ten_volt_task =
            RtosTask<zero_ten_volt_task_stack_size>(zero_ten_volt_task_name,zero_ten_volt_task_priority, zero_ten_input);
    zero_ten_volt_task.create();

    static auto main = MainController(pfc, si, zero_ten_input, pfc::Logger::getInstance());
    static auto main_controller_task =
            RtosTask<main_controller_task_stack_size>(main_controller_task_name,main_controller_task_priority, main);
    main_controller_task.create();

    osKernelStart();

    return 0;
}