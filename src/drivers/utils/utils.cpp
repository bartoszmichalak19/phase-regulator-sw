#include "utils.hpp"
#include "cmsis_os.h"
#include "main.h"

void Utils::osDelay(uint32_t delay_ms)
{
   vTaskDelay(delay_ms);
}
void Utils::deviceDelay(uint32_t delay_ms)
{
   HAL_Delay(delay_ms);
}

void Utils::osYield()
{
   taskYIELD();
}