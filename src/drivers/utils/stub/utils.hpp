#ifndef UTILS_STUB_HPP
#define UTILS_STUB_HPP

#include <stdint.h>

class Utils
{
public:
    static void osDelay(uint32_t delay_ms){};
    static void deviceDelay(uint32_t delay_ms){};
    static void osYield(){};
    static const unsigned long kMaxDelay = 0xffffffffUL;
};

#endif
