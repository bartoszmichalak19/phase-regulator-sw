#ifndef ADC_MOCK_H
#define ADC_MOCK_H

#include "gmock/gmock.h"
#include "i_adc.hpp"


class AdcMock  : public IAdc
{
public:
    MOCK_METHOD(bool, init, (), (override));
    MOCK_METHOD(etl::optional<uint32_t>, getMilivolts, (), (override));

};

#endif
