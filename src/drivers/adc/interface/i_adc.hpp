#ifndef ADC_INTERFACE_H
#define ADC_INTERFACE_H

#include "etl/optional.h"
#include <stdint.h>
#include <tuple>

class IAdc {
public:
    virtual ~IAdc() = default;
    static constexpr uint32_t l_voltage_mv_ = 0;
    static constexpr uint32_t h_voltage_mv_ = 3300;

    virtual bool init() = 0;
    virtual etl::optional <uint32_t> getMilivolts() = 0;
};

#endif
