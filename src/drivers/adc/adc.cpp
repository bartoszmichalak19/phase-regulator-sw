#include "adc.hpp"
#include "utils.hpp"


extern ADC_HandleTypeDef hadc1; // defined bsp/cube/main.c

namespace pfc {

    bool Adc::init() {

        ADC_ChannelConfTypeDef sConfig = {0};
        /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
        */
        sConfig.Channel = channel_;
        sConfig.Rank = 1;
        sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
        if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
            return false;
        }
        return true;
    }

    etl::optional<uint32_t> Adc::getMilivolts() {
        if (!init()) {
            return etl::nullopt;
        }

        if (HAL_ADC_Start(&hadc1) != HAL_OK) {
            return etl::nullopt;
        }

        Utils::osDelay(2);

        if (HAL_ADC_PollForConversion(&hadc1, 10) == HAL_OK) {
            return HAL_ADC_GetValue(&hadc1) * kVref / kAdcMaxValue;
        } else {
            return etl::nullopt;
        }
    }
}