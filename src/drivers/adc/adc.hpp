#ifndef ADC_DRIVER_H
#define ADC_DRIVER_H

#include "i_adc.hpp"
#include "stm32f4xx_hal.h"

namespace pfc {
    class Adc : public IAdc {
    public:
        explicit Adc(uint32_t channel): channel_(channel){}
        bool init() override;
        etl::optional<uint32_t> getMilivolts() override;
    private:
        static constexpr uint32_t kVref = VREFINT_CAL_VREF;
        static constexpr uint32_t kAdcMaxValue = 4095;
        uint32_t channel_;
    };
}
#endif