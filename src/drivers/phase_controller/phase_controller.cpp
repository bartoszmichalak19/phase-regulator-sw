#include "phase_controller.hpp"

extern TIM_HandleTypeDef htim2; //defined bsp/cube/main.c

pfc::Status PhaseController::init()
{
   if (HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2) != HAL_OK)
   {
      return pfc::Status::ERROR;
   }

   return pfc::Status::OK;
}

pfc::Status PhaseController::setPhaseDuty(uint8_t duty)
{  
   duty++;
   return pfc::Status::OK;
}