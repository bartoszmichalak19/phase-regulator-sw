#ifndef PHASE_CONTROLLER_INTERFACE_H
#define PHASE_CONTROLLER_INTERFACE_H

#include <stdint.h>
#include "status.hpp"


class IPhaseController
{
public:
    virtual ~IPhaseController() = default;
    virtual pfc::Status init() = 0;
    virtual pfc::Status setPhaseDuty(uint8_t duty) = 0;
};

#endif
