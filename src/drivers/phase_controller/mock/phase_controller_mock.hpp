#ifndef PHASE_CONTROLLER_MOCK_H
#define PHASE_CONTROLLER_MOCK_H

#include "gmock/gmock.h"
#include "i_phase_controller.hpp"


class PhaseControllerMock  : public IPhaseController
{
public:
    MOCK_METHOD(pfc::Status, init, (), (override));
    MOCK_METHOD(pfc::Status, setPhaseDuty, (uint8_t), (override));
};

#endif
