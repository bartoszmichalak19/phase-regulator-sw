#ifndef PHASE_CONTROLLER_DRIVER_H
#define PHASE_CONTROLLER_DRIVER_H

#include "i_phase_controller.hpp"
#include "main.h"
#include "stm32f4xx_hal_tim.h"

class PhaseController : public IPhaseController
{
private:

public:
    PhaseController() = default;
    ~PhaseController() = default;

    pfc::Status init() override;
    pfc::Status setPhaseDuty(uint8_t duty) override;
};

#endif