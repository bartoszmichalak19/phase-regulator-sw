#ifndef GPIO_INTERFACE_H
#define GPIO_INTERFACE_H

class IGpio
{
public:
    virtual ~IGpio() = default;
    virtual bool init() = 0;
};

#endif
