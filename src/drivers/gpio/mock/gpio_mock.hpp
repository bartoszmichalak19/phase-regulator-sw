#ifndef GPIO_MOCK_H
#define GPIO_MOCK_H

#include "gmock/gmock.h"
#include "i_gpio.hpp"


class GpioMock  : public IGpio
{
public:
    MOCK_METHOD(bool, init, (), (override));
};

#endif
