add_subdirectory(interface)

if(BUILD_UT)
    add_subdirectory(mock)
else(BUILD_UT)
    project(GpioDriver CXX)

    FILE(GLOB SRC *.cpp)
    add_library(${PROJECT_NAME} STATIC ${SRC})
    target_include_directories(${PROJECT_NAME} PUBLIC .)
    target_link_libraries(${PROJECT_NAME} PRIVATE bsp_drivers GpioInterface)
endif(BUILD_UT)