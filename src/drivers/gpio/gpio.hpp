#ifndef GPIO_DRIVER_H
#define GPIO_DRIVER_H

#include "i_gpio.hpp"

class Gpio : public IGpio
{
private:
public:
    bool init() override;
};

#endif