#ifndef STATUS_INDICATOR_INTERFACE_H
#define STATUS_INDICATOR_INTERFACE_H

#include <stdint.h>
#include "status.hpp"
#include "device_status.hpp"

class IStatusIndicator
{
public:
    virtual ~IStatusIndicator() = default;
    virtual pfc::Status init() = 0;
    virtual pfc::Status indicateDeviceStatus(pfc::DeviceStatus state) = 0;
};

#endif
