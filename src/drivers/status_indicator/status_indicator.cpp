#include "status_indicator.hpp"

pfc::Status StatusIndicator::init()
{
   for (int i = 0; i < 50; i++)
   {
      HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
      HAL_Delay(100);
   }
   return pfc::Status::OK;
}

pfc::Status StatusIndicator::indicateDeviceStatus(pfc::DeviceStatus state)
{

   return pfc::Status::OK;
}