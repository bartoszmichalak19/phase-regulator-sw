#ifndef STATUS_INDICATOR_DRIVER_H
#define STATUS_INDICATOR_DRIVER_H

#include "i_status_indicator.hpp"
#include "main.h"

class StatusIndicator : public IStatusIndicator
{
private:
    TIM_HandleTypeDef htim2;

public:
    StatusIndicator() = default;
    ~StatusIndicator() = default;

    pfc::Status init() override;
    pfc::Status indicateDeviceStatus(pfc::DeviceStatus state) override;
};

#endif