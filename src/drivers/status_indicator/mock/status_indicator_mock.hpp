#ifndef UI_MOCK_H
#define UI_MOCK_H

#include "gmock/gmock.h"
#include "i_status_indicator.hpp"


class StatusIndicatorMock  : public IStatusIndicator
{
public:
    MOCK_METHOD(pfc::Status, init, (), (override));
    MOCK_METHOD(pfc::Status, indicateDeviceStatus, (pfc::DeviceStatus), (override));
};

#endif
