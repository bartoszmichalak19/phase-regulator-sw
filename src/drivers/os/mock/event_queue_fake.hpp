#ifndef EVENT_QUEUE_FAKE_H
#define EVENT_QUEUE_FAKE_H

#include "i_event_queue.hpp"
#include <queue>

namespace pfc {
    template<typename EventT>
    class EventQueueFake : public IEventQueue<EventT> {
    public:
        bool send(const EventT &event, uint32_t timeout = 0) override {
            _queue.push(event);
            return true;
        }

        bool receive(EventT &event, uint32_t timeout = 0xffffffffUL) override {
            event = _queue.front();
            _queue.pop();
            return true;
        }

        EventQueueFake() = default;

        ~EventQueueFake() = default;

    private:
        std::queue<EventT> _queue;
    };

}
#endif
