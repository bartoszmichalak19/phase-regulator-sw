#ifndef EVENT_QUEUE_MOCK_H
#define EVENT_QUEUE_MOCK_H

#include "gmock/gmock.h"
#include "i_event_queue.hpp"

namespace pfc {
    template<typename EventT>
    class EventQueueMock : public IEventQueue<EventT> {
    public:
        MOCK_METHOD(bool, send,(const EventT &, uint32_t), (override)

        );

        MOCK_METHOD(bool, receive,(EventT &, uint32_t), (override)

        );
    };

}
#endif
