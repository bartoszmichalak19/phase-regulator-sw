#ifndef RTOS_TASK_HPP
#define RTOS_TASK_HPP

#include <stdint.h>
#include "main.h"
#include "cmsis_os.h"
#include "i_thread.hpp"

template <size_t stack_size>
class RtosTask
{
public:
    RtosTask(const char *task_name, osPriority task_priority, IThread &thread) :
        th(thread), task_name_(task_name), task_priority_(task_priority) {}

    ~RtosTask() = default;

    void create()
    {
        taskHandle_ = xTaskCreateStatic(staticTaskLoop, task_name_,
                                        stack_size_, this, task_priority_,
                                        taskBuffer_.data(), &taskControlBlock_);

        if(taskHandle_ == nullptr){Error_Handler();}
    }

    IThread &getThreadContext()
    {
        return th;
    }

private:
    IThread &th;
    const char *task_name_;
    const size_t stack_size_ = stack_size;
    const osPriority task_priority_;
    osThreadId taskHandle_;
    std::array<uint32_t, stack_size> taskBuffer_;
    osStaticThreadDef_t taskControlBlock_;

    static void staticTaskLoop(void *parameters)
    {
        auto *p_this = static_cast<RtosTask *>(parameters);

        p_this->getThreadContext().setupThread();
        while (true)
        {
            p_this->getThreadContext().mainLoop();
        }
        // should never be here
        Error_Handler();
    }
};

#endif