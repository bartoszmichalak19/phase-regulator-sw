//
// Created by michalak on 20.11.22.
//

#ifndef PHASECONTROLLER_EVENT_QUEUE_HPP
#define PHASECONTROLLER_EVENT_QUEUE_HPP

#include "i_event_queue.hpp"
#include "rtos_queue.hpp"

namespace pfc {
    template<typename EventT, unsigned queueLength>
    class EventQueue : public IEventQueue<EventT> {
    public:
        Queue<EventT, queueLength> &get_queue() { return _queue; }

        bool send(const EventT &event, uint32_t timeout = 0) override { return _queue.add(event, timeout); }

        bool receive(EventT &event, uint32_t timeout = 0xffffffffUL) override { return _queue.pop(event, timeout); }

        EventQueue() {}

        ~EventQueue() {}

    private:
        Queue<EventT, queueLength> _queue;
    };
}
#endif //PHASECONTROLLER_EVENT_QUEUE_HPP
