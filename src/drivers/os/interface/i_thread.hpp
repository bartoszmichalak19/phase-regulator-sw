#ifndef RTOS_THREAD_INTERFACE_H
#define RTOS_THREAD_INTERFACE_H

#include <stdint.h>

class IThread
{
public:
    virtual ~IThread() = default;
    virtual void setupThread() = 0;
    virtual void mainLoop() = 0;
    virtual int getStatus() = 0;
};

#endif
