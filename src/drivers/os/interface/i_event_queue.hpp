//
// Created by michalak on 20.11.22.
//

#ifndef PHASECONTROLLER_I_EVENT_QUEUE_HPP
#define PHASECONTROLLER_I_EVENT_QUEUE_HPP

#include <stdint.h>

namespace pfc {

    template<typename EventT>
    class IEventQueue {
    public:

        virtual bool send(const EventT &event, uint32_t timeout = 0) = 0;


        virtual bool receive(EventT &event, uint32_t timeout = 0xffffffffUL) = 0;
    };
}

#endif //PHASECONTROLLER_I_EVENT_QUEUE_HPP
