#ifndef LOGS_TRANSMITTER_DRIVER_HPP
#define LOGS_TRANSMITTER_DRIVER_HPP

#include "i_logs_transmitter.hpp"

namespace pfc
{   
    class LogsTransmitter : public ILogsTransmitter
    {
    public:
        bool sendSign(char sign) override;
    };
}

#endif