#include "logger.hpp"

using namespace pfc;

Logger &Logger::getInstance()
{
    static Logger instance;
    return instance;
}

Logger::Logger()
{
}

uint16_t Logger::addLogLevelPrefix(LogLevel l_lev)
{
    uint16_t prefix_len = 0;
    switch (l_lev)
    {
    case LogLevel::kDebug:
        prefix_len = snprintf(single_log_buf.data(), kMaxSingleLogLen, "\033[%d;1m[DEBUG] \033[0m", static_cast<int>(l_lev));
        break;

    case LogLevel::kInfo:
        prefix_len = snprintf(single_log_buf.data(), kMaxSingleLogLen, "\033[%d;1m[INFO] \033[0m", static_cast<int>(l_lev));
        break;

    case LogLevel::kError:
        prefix_len = snprintf(single_log_buf.data(), kMaxSingleLogLen, "\033[%d;1m[ERROR] \033[0m", static_cast<int>(l_lev));
        break;
    }
    return prefix_len;
}

void Logger::vprint(LogLevel l_lev, const char *fmt, va_list argp)
{
    uint16_t prefix_len = addLogLevelPrefix(l_lev);
    uint16_t free_space_for_log = kMaxSingleLogLen - (kLenOfLogSuffix + prefix_len);
    if (0 < vsnprintf(single_log_buf.data()+prefix_len, free_space_for_log, fmt, argp))
    {
        strncat(single_log_buf.data(), kLogSuffix, kLenOfLogSuffix);
        uint8_t cnt = 0;
        while (single_log_buf[cnt] != '\0')
        {
            const uint8_t q_push_timeout = 1; // ms
            logs_q_.add(single_log_buf[cnt], q_push_timeout);
            cnt++;
        }
    }
}

void Logger::debug(const char *format, ...)
{
    va_list argp;
    va_start(argp, format);
    vprint(LogLevel::kDebug, format, argp);
    va_end(argp);
}
void Logger::info(const char *format, ...)
{
    va_list argp;
    va_start(argp, format);
    vprint(LogLevel::kInfo, format, argp);
    va_end(argp);
}
void Logger::error(const char *format, ...)
{
    va_list argp;
    va_start(argp, format);
    vprint(LogLevel::kError, format, argp);
    va_end(argp);
}

Queue<char, kMaxLogCharBufferSize> &Logger::getLogsQueue()
{
    return logs_q_;
}
