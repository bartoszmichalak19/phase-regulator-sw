#include "logs_transmitter.hpp"
#include "stm32f4xx_hal.h"


extern UART_HandleTypeDef huart1; // defined bsp/cube/main.c

using namespace pfc;

bool LogsTransmitter::sendSign(char sign) {
    if (const uint32_t timeout_ms = 1;
            HAL_UART_Transmit(&huart1, reinterpret_cast<uint8_t *>(&sign), sizeof(char), timeout_ms) != HAL_OK) {
        return false;
    }
    return true;
}