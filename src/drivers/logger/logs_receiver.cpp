#include "logs_receiver.hpp"


bool pfc::LogsReceiver::waitForSign(char& sign)
{   
    return logs_q_.pop(sign);
}