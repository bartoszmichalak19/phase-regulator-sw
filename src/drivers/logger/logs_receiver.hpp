#ifndef LOGs_RECEIVER_DRIVER_HPP
#define LOGs_RECEIVER_DRIVER_HPP

#include "i_logs_receiver.hpp"
#include "rtos_queue.hpp"
#include "logger.hpp"

namespace pfc
{

    class LogsReceiver : public ILogsReceiver
    {
    public:
        LogsReceiver(Queue<char, kMaxLogCharBufferSize> &logs_queue) : logs_q_(logs_queue) {}
        bool waitForSign(char& sign) override;

    private:
        Queue<char, kMaxLogCharBufferSize> &logs_q_;
    };
}

#endif