#ifndef LOGS_RECEIVER_MOCK_H
#define LOGS_RECEIVER_MOCK_H

#include "gmock/gmock.h"
#include "i_logs_receiver.hpp"


class LogsReceiverMock  : public ILogsReceiver
{
public: 
    MOCK_METHOD(bool, waitForSign, (char& sign), (override));
};

#endif
