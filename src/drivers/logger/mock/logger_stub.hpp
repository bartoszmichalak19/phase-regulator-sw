#ifndef LOGGER_STUB_H
#define LOGGER_STUB_H

#include "gmock/gmock.h"
#include "i_logger.hpp"

class LoggerStub : public ILogger
{
public:
    void debug(const char *format, ...) override {};
    void info(const char *format, ...) override {};
    void error(const char *format, ...) override {};
};

#endif
