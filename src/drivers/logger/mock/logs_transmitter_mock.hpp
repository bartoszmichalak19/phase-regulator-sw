#ifndef LOGS_TRANSMITTER_MOCK_H
#define LOGS_TRANSMITTER_MOCK_H

#include "gmock/gmock.h"
#include "i_logs_transmitter.hpp"


class LogsTransmitterMock  : public ILogsTransmitter
{
public:
    MOCK_METHOD(bool, sendSign, (char sign), (override));
};

#endif
