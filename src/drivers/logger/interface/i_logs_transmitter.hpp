#ifndef LOGS_TRANSMITTER_INTERFACE_H
#define LOGS_TRANSMITTER_INTERFACE_H

class ILogsTransmitter
{
public:
    virtual ~ILogsTransmitter() = default;
    virtual bool sendSign(char sign) = 0;
};

#endif
