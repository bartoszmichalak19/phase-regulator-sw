#ifndef LOGS_RECEIVER_INTERFACE_H
#define LOGS_RECEIVER_INTERFACE_H

class ILogsReceiver
{
public:
    virtual ~ILogsReceiver() = default;
    virtual bool waitForSign(char& sign) = 0;
};

#endif
