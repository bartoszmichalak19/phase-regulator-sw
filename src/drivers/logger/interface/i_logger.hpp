#ifndef LOGGER_INTERFACE_H
#define LOGGER_INTERFACE_H

class ILogger
{
public:
    virtual ~ILogger() = default;
    virtual void debug(const char *format, ...) = 0;
    virtual void info(const char *format, ...) = 0;
    virtual void error(const char *format, ...) = 0;
};

#endif
