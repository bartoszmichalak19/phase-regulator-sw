#ifndef LOGGER_DRIVER_HPP
#define LOGGER_DRIVER_HPP

#include "i_logger.hpp"
#include "rtos_queue.hpp"
#include <array>
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

namespace pfc
{
    constexpr uint16_t kMaxSingleLogLen = 128;
    constexpr uint16_t kMaxLogCharBufferSize = 512;

    constexpr int kRedFontColor = 31;
    constexpr int kBlueFontColor = 32;
    constexpr int kGreenFontColor = 34;

    constexpr const char *kLogSuffix = "\n\r";
    constexpr int kLenOfLogSuffix = 3;

    enum class LogLevel
    {
        kDebug = kBlueFontColor,
        kInfo = kGreenFontColor,
        kError = kRedFontColor
    };

    class Logger : public ILogger
    {
    public:
        static Logger &getInstance();
        Logger operator=(Logger const &) = delete;

        void debug(const char *format, ...) override;
        void info(const char *format, ...) override;
        void error(const char *format, ...) override;

        Queue<char, kMaxLogCharBufferSize> &getLogsQueue();

    private:
        Logger();

        Queue<char, kMaxLogCharBufferSize> logs_q_;
        std::array<char, kMaxSingleLogLen> single_log_buf;
        uint16_t addLogLevelPrefix(LogLevel l_lev);
        void vprint(LogLevel l_lev, const char *fmt, va_list argp);
        void debug_printf(LogLevel l_lev, const char *fmt, ...);
    };
}

#endif