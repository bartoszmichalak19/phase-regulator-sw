#ifndef INPUT_SIGNAL_INTERFACE_H
#define INPUT_SIGNAL_INTERFACE_H

#include "setpoint.hpp"
#include "etl/optional.h"
#include "i_thread.hpp"

namespace pfc {
    enum class InputSignalThreadStatus
    {
        kFault = -1,
        kRunning = 0,
        kWaiting = 1
    };

    class IInputSignal: public IThread {
    public:
        virtual ~IInputSignal() = default;

        virtual etl::optional<Setpoint> receiveEfficiencySetpoint() = 0;

        virtual int getInputDeviceStatus() = 0;
    };
}
#endif
