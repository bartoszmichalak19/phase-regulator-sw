//
// Created by michalak on 09.01.23.
//

#ifndef PHASECONTROLLER_SIGNAL_UTILS_H
#define PHASECONTROLLER_SIGNAL_UTILS_H

#include <stdint.h>

uint64_t map(uint64_t x, uint64_t in_min, uint64_t in_max, uint64_t out_min, uint64_t out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#endif //PHASECONTROLLER_SIGNAL_UTILS_H
