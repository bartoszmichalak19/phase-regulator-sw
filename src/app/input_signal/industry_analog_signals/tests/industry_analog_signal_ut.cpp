
#include <memory>
#include "gtest/gtest.h"
#include "zero_ten_volt_input.hpp"
#include "logger_stub.hpp"
#include "adc_mock.hpp"
#include "event_queue_mock.hpp"
#include "event_queue_fake.hpp"

using ::testing::Test;
using ::testing::_;
using ::testing::Return;

using namespace pfc;

    class InputSignalTests : public Test {
    protected:
        LoggerStub log;
        AdcMock adc;
        EventQueueFake<uint8_t> ev_queue;
        IEventQueue<uint8_t>& ev_q = ev_queue;
        ZeroTenVoltInput zero_ten_v_signal = ZeroTenVoltInput(adc, log, ev_q);
    };

TEST_F(InputSignalTests, successfulSetup) {
    EXPECT_CALL(adc, init()).WillOnce(Return(true));

    zero_ten_v_signal.setupThread();

    ASSERT_EQ(static_cast<ZeroTenVoltDeviceStatus>(zero_ten_v_signal.getInputDeviceStatus()), ZeroTenVoltDeviceStatus::kOk);
    ASSERT_EQ(static_cast<InputSignalThreadStatus>(zero_ten_v_signal.getStatus()), InputSignalThreadStatus::kRunning);
}

TEST_F(InputSignalTests, notSuccessfulSetup) {
    EXPECT_CALL(adc, init()).WillOnce(Return(false));

    zero_ten_v_signal.setupThread();

    ASSERT_EQ(static_cast<ZeroTenVoltDeviceStatus>(zero_ten_v_signal.getInputDeviceStatus()), ZeroTenVoltDeviceStatus::kAdcFault);
    ASSERT_EQ(static_cast<InputSignalThreadStatus>(zero_ten_v_signal.getStatus()), InputSignalThreadStatus::kFault);
}

TEST_F(InputSignalTests, signalInMiddleOfAdcRange) {
    EXPECT_CALL(adc, getMilivolts()).WillOnce(Return(1650));

    zero_ten_v_signal.mainLoop();

    ASSERT_EQ(zero_ten_v_signal.receiveEfficiencySetpoint().value().get(), 62);
}

TEST_F(InputSignalTests, signalInMaxOfAdcRange) {
    EXPECT_CALL(adc, getMilivolts()).WillOnce(Return(3300));

    zero_ten_v_signal.mainLoop();

    ASSERT_EQ(zero_ten_v_signal.receiveEfficiencySetpoint().value().get(), 100);
}

TEST_F(InputSignalTests, signalInMinOfAdcRange) {
    EXPECT_CALL(adc, getMilivolts()).WillOnce(Return(0));

    zero_ten_v_signal.mainLoop();

    ASSERT_EQ(zero_ten_v_signal.receiveEfficiencySetpoint().value().get(), 0);
}

TEST_F(InputSignalTests, adcReadFail) {
    EXPECT_CALL(adc, getMilivolts()).WillOnce(Return(etl::nullopt));

    zero_ten_v_signal.mainLoop();

    ASSERT_EQ(zero_ten_v_signal.receiveEfficiencySetpoint().value().get(), 0);
    ASSERT_EQ(static_cast<ZeroTenVoltDeviceStatus>(zero_ten_v_signal.getInputDeviceStatus()), ZeroTenVoltDeviceStatus::kAdcFault);
    ASSERT_EQ(static_cast<InputSignalThreadStatus>(zero_ten_v_signal.getStatus()), InputSignalThreadStatus::kFault);
}

TEST_F(InputSignalTests, queueReceiveFail) {
    EventQueueMock<uint8_t> ev_q_mock;
    IEventQueue<uint8_t>& ev_q_mocked = ev_q_mock;

    ZeroTenVoltInput zero_ten_v_signal2 = ZeroTenVoltInput(adc, log, ev_q_mocked);

    EXPECT_CALL(ev_q_mock, receive(_,_)).WillOnce(Return(false));
    ASSERT_EQ(zero_ten_v_signal2.receiveEfficiencySetpoint(), etl::nullopt);
}

