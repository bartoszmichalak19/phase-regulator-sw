#include "zero_ten_volt_input.hpp"
#include "utils.hpp"

namespace pfc {
    void ZeroTenVoltInput::setupThread() {
        logger_.info("Init zero ten volt input thread");
        if (adc_.init()) {
            input_status_ = ZeroTenVoltDeviceStatus::kOk;
            thread_satus_ = InputSignalThreadStatus::kRunning;
        } else {
            input_status_ = ZeroTenVoltDeviceStatus::kAdcFault;
            thread_satus_ = InputSignalThreadStatus::kFault;
        }
    }

    void ZeroTenVoltInput::mainLoop() {
        float signal = .0f;
        if (auto adc_mv = adc_.getMilivolts()) {
            signal = static_cast<float>(adc_mv.value()) / v_divider_.getRatio();
            thread_satus_ = InputSignalThreadStatus::kWaiting;
            logger_.debug("0-10V read adc: %d", static_cast<uint32_t>(signal));

        } else {
            input_status_ = ZeroTenVoltDeviceStatus::kAdcFault;
            thread_satus_ = InputSignalThreadStatus::kFault;
            logger_.debug("0-10V read adc fail");
        }
        auto setpoint = static_cast<uint8_t>(signal / 100);
        setpoints_q_.send(setpoint);
        Utils::osDelay(sampling_period_ms_);

    }

    etl::optional<Setpoint> ZeroTenVoltInput::receiveEfficiencySetpoint() {
        uint8_t value;
        if (setpoints_q_.receive(value)) {
            return Setpoint(value);
        } else {
            return etl::nullopt;
        }
    }
}