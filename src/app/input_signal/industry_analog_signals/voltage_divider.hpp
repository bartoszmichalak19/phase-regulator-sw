//
// Created by michalak on 19.11.22.
//

#ifndef PHASECONTROLLER_VOLTAGE_DIVIDER_HPP
#define PHASECONTROLLER_VOLTAGE_DIVIDER_HPP

#include <stdint.h>

namespace pfc {
    struct VoltageDivider {
        VoltageDivider(float r1, float r2) : r1_(r1), r2_(r2) {}

        const float getRatio() { return r2_ / (r1_ + r2_); }

    private:
        const float r1_;
        const float r2_;
    };
}
#endif //PHASECONTROLLER_VOLTAGE_DIVIDER_HPP
