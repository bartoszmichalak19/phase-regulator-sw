#ifndef ZERO_TEN_VOLT_INPUT_HPP
#define ZERO_TEN_VOLT_INPUT_HPP


#include "i_logger.hpp"
#include "i_input_signal.hpp"
#include "i_adc.hpp"
#include "voltage_divider.hpp"
#include "i_event_queue.hpp"

namespace pfc {

    enum class ZeroTenVoltDeviceStatus {
        kOutOfRange = -3,
        kAdcFault = -2,
        kFault = -1,
        kOk = 0,
        kUninitialized = 1
    };
    using SetpointsQueue = IEventQueue<uint8_t>;

    class ZeroTenVoltInput : public IInputSignal {
        static constexpr float resistor1_ = 10.0f;
        static constexpr float resistor2_ = 3.6f;
    public:
        ZeroTenVoltInput(IAdc &adc, ILogger &logger, SetpointsQueue &setpoints_q,
                         VoltageDivider v_divider = VoltageDivider(resistor1_, resistor2_))
                : adc_(adc), logger_(logger), v_divider_(v_divider), setpoints_q_(setpoints_q) {}

        void setupThread() override;

        void mainLoop() override;

        int getStatus() override { return static_cast<int>(thread_satus_); };

        /**
         *
         * @return Setpoint when succesfully received value otherwise nullopt
         */
        etl::optional<Setpoint> receiveEfficiencySetpoint() override;

        int getInputDeviceStatus() override { return static_cast<int>(input_status_); };

    private:
        IAdc &adc_;
        ILogger &logger_;
        VoltageDivider v_divider_;
        ZeroTenVoltDeviceStatus input_status_ = ZeroTenVoltDeviceStatus::kUninitialized;
        InputSignalThreadStatus thread_satus_ = InputSignalThreadStatus::kWaiting;
        SetpointsQueue &setpoints_q_;
        const uint32_t sampling_period_ms_ = 1000;
    };
}
#endif