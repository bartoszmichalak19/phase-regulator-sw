#ifndef INPUT_SIGNAL_MOCK_H
#define INPUT_SIGNAL_MOCK_H

#include "gmock/gmock.h"
#include "i_input_signal.hpp"
#include "setpoint.hpp"


class InputSignalMock  : public pfc::IInputSignal
{
public:
    MOCK_METHOD(etl::optional<pfc::Setpoint>, receiveEfficiencySetpoint, (), (override));
    MOCK_METHOD(int, getInputDeviceStatus, (), (override));
    MOCK_METHOD(void, setupThread, (), (override));
    MOCK_METHOD(void, mainLoop, (), (override));
    MOCK_METHOD(int, getStatus, (), (override));
};

#endif
