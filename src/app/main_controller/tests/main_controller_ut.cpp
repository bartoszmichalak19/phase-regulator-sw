#include "gtest/gtest.h"
#include "main_controller.hpp"
#include "phase_controller_mock.hpp"
#include "status_indicator_mock.hpp"
#include "input_signal_mock.hpp"
#include "logger_stub.hpp"


using ::testing::Test;
using ::testing::_;
using ::testing::Return;

class MainControllerTests : public Test
{
protected:
    PhaseControllerMock pfc;
    StatusIndicatorMock si;
    InputSignalMock input;
    LoggerStub log;
    MainController main = MainController(pfc, si, input, log);
};

TEST_F(MainControllerTests, initSuccess)
{
    EXPECT_CALL(si, init()).WillOnce(Return(pfc::Status::OK));
    EXPECT_CALL(pfc, init()).WillOnce(Return(pfc::Status::OK));
    EXPECT_CALL(input, getInputDeviceStatus()).WillOnce(Return(0));
    EXPECT_CALL(si, indicateDeviceStatus(pfc::DeviceStatus::OK));

    auto result = main.baseInit();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kWaiting);
    ASSERT_EQ(result, pfc::Status::OK);
}

TEST_F(MainControllerTests, initFirstModuleFailBreaksExecution)
{
    EXPECT_CALL(si, init()).WillOnce(Return(pfc::Status::ERROR));
    EXPECT_CALL(si, indicateDeviceStatus(pfc::DeviceStatus::ERROR));
    EXPECT_CALL(pfc, init()).Times(0);
    EXPECT_CALL(input, getInputDeviceStatus()).Times(0);

    auto result = main.baseInit();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kFault);
    ASSERT_EQ(result, pfc::Status::ERROR);
}

TEST_F(MainControllerTests, initSecondModuleFailBreaksExecution)
{
    EXPECT_CALL(si, init()).WillOnce(Return(pfc::Status::OK));
    EXPECT_CALL(pfc, init()).WillOnce(Return(pfc::Status::ERROR));
    EXPECT_CALL(si, indicateDeviceStatus(pfc::DeviceStatus::ERROR));
    EXPECT_CALL(input, getInputDeviceStatus()).Times(0);

    auto result = main.baseInit();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kFault);
    ASSERT_EQ(result, pfc::Status::ERROR);
}

TEST_F(MainControllerTests, initThirdModuleFailBreaksExecution)
{
    EXPECT_CALL(si, init()).WillOnce(Return(pfc::Status::OK));
    EXPECT_CALL(pfc, init()).WillOnce(Return(pfc::Status::OK));
    EXPECT_CALL(input, getInputDeviceStatus()).WillOnce(Return(-1));
    EXPECT_CALL(si, indicateDeviceStatus(pfc::DeviceStatus::ERROR));


    auto result = main.baseInit();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kFault);
    ASSERT_EQ(result, pfc::Status::ERROR);
}

TEST_F(MainControllerTests, controlDeviceWhenSetpointReceived)
{
    EXPECT_CALL(input, receiveEfficiencySetpoint()).WillOnce(Return(pfc::Setpoint(55)));

    main.controlDevice();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kWaiting);
}

TEST_F(MainControllerTests, controlDeviceWhenSetpointReceivedIsWrong)
{
    EXPECT_CALL(input, receiveEfficiencySetpoint()).WillOnce(Return(etl::nullopt));

    main.controlDevice();

    ASSERT_EQ(static_cast<MainControllerThreadStatus>(main.getStatus()), MainControllerThreadStatus::kWaiting);
}