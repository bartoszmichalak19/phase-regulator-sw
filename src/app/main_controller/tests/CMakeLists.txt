add_executable(
        main_controller_ut
        main_controller_ut.cpp
)
target_link_libraries(
        main_controller_ut
        gtest_main
        gmock
        MainController
        PhaseControllerMock
        StatusIndicatorMock
        InputSignalMock
        LoggerMock
)

gtest_discover_tests(
        main_controller_ut
)