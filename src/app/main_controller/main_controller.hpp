#ifndef MAIN_CONTROLLER_H
#define MAIN_CONTROLLER_H

#include "i_phase_controller.hpp"
#include "i_status_indicator.hpp"
#include "i_input_signal.hpp"
#include "i_thread.hpp"
#include "i_logger.hpp"


enum class MainControllerThreadStatus {
    kFault = -1,
    kRunning = 0,
    kWaiting = 1
};

class MainController : public IThread {
public:
    MainController(IPhaseController &pfc, IStatusIndicator &si, pfc::IInputSignal &input,  ILogger &logger) : pfc_(pfc), si_(si),
                                                                                            input_(input), logger_(logger) {}

    MainController() = delete;
    ~MainController() = default;
    pfc::Status baseInit();
    void controlDevice();

    void setupThread() override { baseInit(); }
    void mainLoop() override { controlDevice(); }
    int getStatus() override { return static_cast<int>(thread_satus_); };

private:
    static constexpr uint32_t kDeviceStartupDelayMs = 1000;
    MainControllerThreadStatus thread_satus_ = MainControllerThreadStatus::kWaiting;
    IPhaseController &pfc_;
    IStatusIndicator &si_;
    pfc::IInputSignal &input_;
    ILogger &logger_;
};

#endif
