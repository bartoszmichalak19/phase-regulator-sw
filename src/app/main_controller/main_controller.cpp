#include "main_controller.hpp"
#include "utils.hpp"

pfc::Status MainController::baseInit() {
    Utils::osDelay(kDeviceStartupDelayMs);
    pfc::Status result;

    result = si_.init();
    if (result != pfc::Status::OK) {
        thread_satus_ = MainControllerThreadStatus::kFault;
        si_.indicateDeviceStatus(pfc::DeviceStatus::ERROR);
        return result;
    }

    result = pfc_.init();
    if (result != pfc::Status::OK) {
        thread_satus_ = MainControllerThreadStatus::kFault;
        si_.indicateDeviceStatus(pfc::DeviceStatus::ERROR);
        return result;
    }

    if (input_.getInputDeviceStatus() != static_cast<int>(pfc::DeviceStatus::OK)) {
        thread_satus_ = MainControllerThreadStatus::kFault;
        si_.indicateDeviceStatus(pfc::DeviceStatus::ERROR);
        return pfc::Status::ERROR;
    }

    si_.indicateDeviceStatus(pfc::DeviceStatus::OK);
    return result;
}

void MainController::controlDevice() {
    if (auto setpoint = input_.receiveEfficiencySetpoint()) {
        thread_satus_ = MainControllerThreadStatus::kRunning;
        pfc_.setPhaseDuty(setpoint.value().get());
        logger_.debug("Setting phase duty %d", setpoint.value().get());
        thread_satus_ = MainControllerThreadStatus::kWaiting;
    }

}