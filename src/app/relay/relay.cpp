#include "relay.hpp"

Relay::Relay(IGpio &gpio) : gpio_{gpio} {}

bool Relay::init()
{
    return gpio_.init();
}