#ifndef RELAY_H
#define RELAY_H

#include "i_gpio.hpp"

class Relay
{
public:
    Relay(IGpio& gpio);
    bool init();

private:
    IGpio& gpio_;
};

#endif