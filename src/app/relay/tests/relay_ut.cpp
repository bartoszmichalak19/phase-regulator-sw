#include <memory>
#include "gtest/gtest.h"
#include "relay.hpp"
#include "gpio_mock.hpp"

using ::testing::Test;
using ::testing::Return;

class RelayTests : public Test
{
protected:
    GpioMock gpio;
    Relay rel = Relay(gpio);
};

TEST_F(RelayTests, relayInitSuccess)
{
    EXPECT_CALL(gpio, init()).WillOnce(Return(true));
    EXPECT_TRUE(rel.init());
}

TEST_F(RelayTests, relayInitFail)
{
    EXPECT_CALL(gpio, init()).WillOnce(Return(false));
    EXPECT_FALSE(rel.init());
}