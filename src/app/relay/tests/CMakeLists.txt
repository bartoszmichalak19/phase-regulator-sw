add_executable(
        relay_ut
        relay_ut.cpp
)
target_link_libraries(
        relay_ut
        gtest_main
        gmock
        Relay
        GpioMock
)

gtest_discover_tests(
        relay_ut
)