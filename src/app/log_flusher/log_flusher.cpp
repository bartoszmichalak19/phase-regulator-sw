#include "log_flusher.hpp"
#include "utils.hpp"

void LogFlusher::setupThread()
{
    logger_.info("Init log flusher task");
}

void LogFlusher::mainLoop()
{
    char sign;
    if (logs_rx_.waitForSign(sign) && status_ == LogFlusherStatus::kWaitingForLogs)
    {
        status_ = LogFlusherStatus::kSendingLogs;
        while (not logs_tx_.sendSign(sign) && status_ == LogFlusherStatus::kSendingLogs)
        {
            if (retransmissionCounter >= kRetransmissionFaultTreshold)
            {
                status_ = LogFlusherStatus::kTransmitterFault;
                Utils::osDelay(Utils::kMaxDelay);
                return;
            }
            else
            {
                retransmissionCounter++;
                Utils::osDelay(1);
            }
        }
        retransmissionCounter = 0;
        status_ = LogFlusherStatus::kWaitingForLogs;
    }
}

int LogFlusher::getStatus()
{
    return static_cast<int>(status_);
}