
#include <memory>
#include "gtest/gtest.h"
#include "log_flusher.hpp"
#include "logs_receiver_mock.hpp"
#include "logs_transmitter_mock.hpp"
#include "logger_stub.hpp"


using ::testing::Test;
using ::testing::_;
using ::testing::Return;

class LogFlusherTests : public Test
{
protected:
    LogsTransmitterMock logs_tx;
    LogsReceiverMock logs_rx;
    LoggerStub log;
    LogFlusher log_flush = LogFlusher(logs_rx, logs_tx, log);
};

TEST_F(LogFlusherTests, AfterCreateStatus)
{   
    EXPECT_EQ(static_cast<int>(LogFlusherStatus::kWaitingForLogs), log_flush.getStatus());
}

TEST_F(LogFlusherTests, SuccessFlushOneSign)
{   
    EXPECT_CALL(logs_rx, waitForSign(_)).WillOnce(Return(true));
    EXPECT_CALL(logs_tx, sendSign(_)).WillOnce(Return(true));
    log_flush.mainLoop();
    EXPECT_EQ(static_cast<int>(LogFlusherStatus::kWaitingForLogs), log_flush.getStatus());
}

TEST_F(LogFlusherTests, willRetransmitOnSendFail)
{   
    EXPECT_CALL(logs_rx, waitForSign(_)).WillOnce(Return(true));
    EXPECT_CALL(logs_tx, sendSign(_))
    .WillOnce(Return(false))
    .WillOnce(Return(true));
    log_flush.mainLoop();
    EXPECT_EQ(static_cast<int>(LogFlusherStatus::kWaitingForLogs), log_flush.getStatus());
}

TEST_F(LogFlusherTests, willRetransmitSpecifiedTimesOnSendFail)
{   
    const int retransmission_treshold = 50;
    EXPECT_CALL(logs_rx, waitForSign(_)).WillOnce(Return(true));
    ON_CALL(logs_tx, sendSign(_)).WillByDefault(Return(false));
    EXPECT_CALL(logs_tx, sendSign(_)).Times(retransmission_treshold+1);
    log_flush.mainLoop();
    EXPECT_EQ(static_cast<int>(LogFlusherStatus::kTransmitterFault), log_flush.getStatus());
}