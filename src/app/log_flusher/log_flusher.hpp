#ifndef LOG_FLUSHER_HPP
#define LOG_FLUSHER_HPP

#include "i_thread.hpp"
#include "i_logs_receiver.hpp"
#include "i_logs_transmitter.hpp"
#include "i_logger.hpp"

enum class LogFlusherStatus
{   
    kTransmitterFault = -1,
    kWaitingForLogs = 0,
    kSendingLogs = 1
};
class LogFlusher : public IThread
{
public:
    LogFlusher(ILogsReceiver &logs_rx, ILogsTransmitter &logs_tx, ILogger &logger) : 
    logs_rx_(logs_rx), logs_tx_(logs_tx), logger_(logger) {}

    void setupThread() override;
    void mainLoop() override;
    int getStatus() override;

private:
    const uint8_t kRetransmissionFaultTreshold = 50;
    uint8_t retransmissionCounter = 0;
    LogFlusherStatus status_ = LogFlusherStatus::kWaitingForLogs;
    ILogsReceiver &logs_rx_;
    ILogsTransmitter &logs_tx_;
    ILogger &logger_;
};

#endif