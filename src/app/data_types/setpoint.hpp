//
// Created by michalak on 17.11.22.
//

#ifndef PHASECONTROLLER_SETPOINT_HPP
#define PHASECONTROLLER_SETPOINT_HPP

#include <stdint.h>
#include <algorithm>

namespace pfc {
    struct Setpoint {
        Setpoint(uint8_t value) { _setpoint = std::clamp(static_cast<int>(value), 0, 100); }

        uint8_t get() { return _setpoint; }

    private:
        uint8_t _setpoint = 0;
    };
}
#endif //PHASECONTROLLER_SETPOINT_HPP
