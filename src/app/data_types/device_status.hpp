#ifndef DEVICE_STATUS_H
#define DEVICE_STATUS_H

namespace pfc
{
    enum class DeviceStatus
    {
        OK,
        ERROR,
        UNDEFINED
    };

}

#endif