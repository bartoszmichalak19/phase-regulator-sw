#ifndef STATUS_H
#define STATUS_H
namespace pfc
{
    enum class Status
    {
        OK,
        ERROR,
        INPUT_ERROR,
        OUTPUT_ERROR
    };

}

#endif