#!/bin/bash
mkdir -p build
mkdir -p build/debug
cmake -B build/debug -DCMAKE_BUILD_TYPE=Debug
cd build/debug
make -j8 all

